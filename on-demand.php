<?php

/**
 * Employee Scheduler On Demand
 *
 * @wordpress-plugin
 * Plugin Name:       Employee Scheduler On Demand
 * Plugin URI:        https://employee-scheduler.co
 * Description:       Extends Employee Scheduler to allow customers to request services from employees
 * Version:           1.0.0
 * Author:            Morgan Kay
 * Author URI:        https://wpalchemists.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       employee-scheduler-on-demand
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-employee-scheduler-on-demand-activator.php
 */
function activate_on_demand() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-on-demand-activator.php';
	register_activation_hook(   __FILE__, array( 'On_Demand_Activator', 'on_activation' ) );
	On_Demand_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-employee-scheduler-on-demand-deactivator.php
 */
function deactivate_on_demand() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-on-demand-deactivator.php';
	On_Demand_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_on_demand' );
register_deactivation_hook( __FILE__, 'deactivate_on_demand' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-on-demand.php';



/**
 * Check that Employee Scheduler is installed and active
 */
add_action( 'admin_init', 'require_employee_scheduler' );
function require_employee_scheduler() {

	if ( is_admin() && current_user_can( 'activate_plugins' ) && ( !is_plugin_active( 'employee-scheduler/employee-scheduler.php' ) || !is_plugin_active( 'employee-scheduler-pro/employee-scheduler-pro.php' ) ) ) {
		add_action( 'admin_notices', 'need_employee_scheduler_notice' );

		deactivate_plugins( plugin_basename( __FILE__ ) );

		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

}

/**
 * Display error message if Employee Scheduler is not active
 */
function need_employee_scheduler_notice() {
	?><div class="error">
	<p>
		<?php printf(
			__( 'Sorry, but Employee Scheduler On Demand requires <a href="%s">Employee Scheduler</a> and <a href="%s">Employee Scheduler Pro</a> to be installed and active.', 'employee-scheduler-on-demand' ),
			'https://wordpress.org/plugins/employee-scheduler/',
			'https://employee-scheduler.co/downloads/employee-scheduler-pro/'
			); ?>
	</p>
	</div><?php
}


/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_on_demand() {

	$plugin = new On_Demand();
	$plugin->run();

}

run_on_demand();
