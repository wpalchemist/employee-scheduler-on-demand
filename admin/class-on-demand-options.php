<?php
/**
 * The options class.
 *
 * Add On Demand options to the Employee Scheduler options page
 *
 * @since      1.0.0
 * @package    On_Demand
 * @subpackage On_Demand/admin
 */

class On_Demand_Options {
	private $esod_options;

	public function __construct() {
		$this->esod_options = get_option( 'wpaesm_options' );
		add_action( 'admin_init', array( $this, 'options_page_init' ) );
	}

	public function options_page_init() {

		add_settings_section(
			'esod_setting_section', // id
			__( 'On Demand Settings', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_section_info' ), // callback
			'wpaesm_plugin_options' // page
		);

		add_settings_field(
			'esod_billing_timing', // id
			__( 'When to Create Invoice?', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_billing_timing_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'description' => __( 'When do you want to crate an invoice for the employer?', 'employee-scheduler-on-demand' ),
			)
		);

      add_settings_field(
			'esod_send_invoice', // id
			__( 'Send Invoice Automatically?', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_send_invoice_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'description' => __( 'Do you want Employee Scheduler to send the invoice automatically as soon as it is created, or do you want to send it manually?', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_billing_method', // id
			__( 'Invoice Calculation', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_billing_method_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'description' => __( 'How do you want to calculate the invoice that the employer recieves?', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_hourly_rate', // id
			__( 'Hourly Rate', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_hourly_rate_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-hourly-rate',
				'description' => __( 'If you calculate invoices by charging a flat hourly rate, enter the rate here.', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_markup', // id
			__( 'Mark-up Percentage', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_markup_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-markup',
				'description' => __( 'If you calculate invoices by marking up employee pay rates, enter the mark-up percentage here.', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_calc', // id
			__( 'Invoice: Requested or Actual', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_calc_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'description' => __( 'Do you want to bill employers based on the time they requested, or on the time the employee actually worked?', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_admin_notification', // id
			__( 'Notify Admins of Shift Requests', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_admin_notification_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-admin-notification',
			)
		);

		add_settings_field(
			'esod_allow_cancellation', // id
			__( 'Allow Employers To Cancel Shifts', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_allow_cancellation_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-allow-cancellation',
				'description' => __( 'Do you want to allow employers to cancel future shifts they have requested?', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_cancellation_deadline', // id
			__( 'Cancellation Deadline', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_cancellation_deadline_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-admin-notification',
				'description' => __( 'How much advance notice, in hours, must employers give to cancel a shift? When the shift falls within this timeframe, the "cancel shift" option will be disabled. Example (for two days\' notice): 48', 'employee-scheduler-on-demand' ),
			)
		);

		add_settings_field(
			'esod_terms', // id
			__( 'Terms and Conditions', 'employee-scheduler-on-demand' ), // title
			array( $this, 'esod_terms_callback' ), // callback
			'wpaesm_plugin_options', // page
			'esod_setting_section', // section
			array(
				'label_for' => 'esod-terms',
				'description' => __( 'If you want employers to agree to terms and conditions before requesting a shift, select the page that contains your terms and conditions.', 'employee-scheduler-on-demand' ),
			)
		);

	}

	public function sanitize_options( $input ) {

		if( isset( $input['esod_billing_timing'] ) ) {
			if( 'after_request' == $input['esod_billing_timing'] ||
			    'after_acceptance' == $input['esod_billing_timing'] ||
			    'after_shift' == $input['esod_billing_timing'] ) {
				$input['esod_billing_timing'] = $input['esod_billing_timing'];
			} else {
				$input['esod_billing_timing'] = '';
			}
		}

		if( isset( $input['esod_send_invoice'] ) ) {
			if( 'automatically' == $input['esod_send_invoice'] || 'manually' == $input['esod_send_invoice'] ) {
				$input['esod_send_invoice'] = $input['esod_send_invoice'];
			} else {
				$input['esod_send_invoice'] = '';
			}
		}

		if( isset( $input['esod_billing_method'] ) ) {
			if( 'flat_hourly' == $input['esod_billing_method'] || 'employee_markup' == $input['esod_billing_method'] ) {
				$input['esod_billing_method'] = $input['esod_billing_method'];
			} else {
				$input['esod_billing_method'] = '';
			}
		}

		if( isset( $input['esod_hourly_rate'] ) ) {
			$input['esod_hourly_rate'] = floatval( $input['esod_hourly_rate'] );
		}

		if( isset( $input['esod_markup'] ) ) {
			$input['esod_markup'] = floatval( $input['esod_markup'] );
		}

		if( isset( $input['esod_calc'] ) ) {
			if( 'requested' == $input['esod_calc'] || 'actual' == $input['esod_calc'] ) {
				$input['esod_calc'] = $input['esod_calc'];
			} else {
				$input['esod_calc'] = '';
			}
		}

		if( isset( $input['esod_admin_notification'] ) ) {
			if( 'notify' == $input['esod_admin_notification'] ) {
				$input['esod_admin_notification'] = $input['esod_admin_notification'];
			} else {
				$input['esod_admin_notification'] = '';
			}
		}

		if( isset( $input['esod_allow_cancellation'] ) ) {
			if( 'allow' == $input['esod_allow_cancellation'] ) {
				$input['esod_allow_cancellation'] = $input['esod_allow_cancellation'];
			} else {
				$input['esod_allow_cancellation'] = '';
			}
		}

		if( isset( $input['esod_cancellation_deadline'] ) ) {
			$input['esod_cancellation_deadline'] = floatval( $input['esod_cancellation_deadline'] );
		}

		if( isset( $input['esod_terms'] ) ) {
			$input['esod_terms'] = intval( $input['esod_terms'] );
		}

		return $input;
	}

	public function esod_section_info() {

	}

	public function esod_billing_timing_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="radio" name='wpaesm_options[esod_billing_timing]' <?php checked( $this->esod_options['esod_billing_timing'], 'after_request' ); ?> value='after_request' id="esod-after-request">
			<label for="esod-after-request"><?php _e( 'As soon as an employer requests a shift', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<p>
			<input type="radio" name='wpaesm_options[esod_billing_timing]' <?php checked( $this->esod_options['esod_billing_timing'], 'after_acceptance' ); ?> value='after_acceptance' id="esod-after-acceptance">
			<label for="esod-after-acceptance"><?php _e( 'As soon as an employee accepts a shift', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<p>
			<input type="radio" name='wpaesm_options[esod_billing_timing]' <?php checked( $this->esod_options['esod_billing_timing'], 'after_shift' ); ?> value='after_shift' id="esod-after-shift">
			<label for="esod-after-shift"><?php _e( 'After the shift has been worked.', 'employee-scheduler-on-demand' ); ?></label><br />
			<span class="description"><?php _e( 'If you want to bill for the actual time worked instead of the time requested, you must choose this option', 'employee-scheduler-on-demand' ); ?></span>
		</p>
		<?php
	}

	public function esod_send_invoice_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="radio" name='wpaesm_options[esod_send_invoice]' <?php checked( $this->esod_options['esod_send_invoice'], 'automatically' ); ?> value='automatically' id="esod-automatically">
			<label for="esod-automatically"><?php _e( 'Send invoice automatically as soon as it is created.', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<p>
			<input type="radio" name='wpaesm_options[esod_send_invoice]' <?php checked( $this->esod_options['esod_send_invoice'], 'manually' ); ?> value='manually' id="esod-manually">
			<label for="esod-manually"><?php _e( 'Do not send invoices: I will send them myself.', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<?php
	}

	public function esod_billing_method_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="radio" name='wpaesm_options[esod_billing_method]' <?php checked( $this->esod_options['esod_billing_method'], 'flat_hourly' ); ?> value='flat_hourly' id="esod-flat-hourly">
			<label for="esod-flat-hourly"><?php _e( 'The same hourly rate for all shifts (enter the hourly rate below).', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<p>
			<input type="radio" name='wpaesm_options[esod_billing_method]' <?php checked( $this->esod_options['esod_billing_method'], 'employee_markup' ); ?> value='employee_markup' id="esod-employee-markup">
			<label for="esod-employee-markup"><?php _e( 'Mark up employee hourly rate by a certain percentage (enter the percentage below)', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<?php
	}

	public function esod_hourly_rate_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="number" step="0.01" name='wpaesm_options[esod_hourly_rate]' value='<?php echo $this->esod_options['esod_hourly_rate']; ?>' id="esod-hourly-rate">
		</p>
		<?php
	}

	public function esod_markup_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="number" step="0.01" name='wpaesm_options[esod_markup]' value='<?php echo $this->esod_options['esod_markup']; ?>' id="esod-markup">
		</p>
		<?php
	}

	public function esod_calc_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="radio" name='wpaesm_options[esod_calc]' <?php checked( $this->esod_options['esod_calc'], 'requested' ); ?> value='requested' id="esod-requested">
			<label for="esod-requested"><?php _e( 'Bill for the time requested', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<p>
			<input type="radio" name='wpaesm_options[esod_calc]' <?php checked( $this->esod_options['esod_calc'], 'actual' ); ?> value='actual' id="esod-actual">
			<label for="esod-actual"><?php _e( 'Bill for the time worked', 'employee-scheduler-on-demand' ); ?></label><br />
			<span class="description"><?php _e( 'If you select this option, the invoice will be sent at the end of the shift.', 'employee-scheduler-on-demand' ); ?></span>
		</p>
		<?php
	}

	public function esod_admin_notification_callback( $args ) {
		?>
		<p>
			<input type="checkbox" name='wpaesm_options[esod_admin_notification]' <?php checked( $this->esod_options['esod_admin_notification'], 'notify' ); ?> value='notify' id="esod-admin-notification">
			<label for="esod-admin-notification"><?php _e( 'Send notification to admin when an employer successfully requests a shift.', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<?php
	}

	public function esod_allow_cancellation_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="checkbox" name='wpaesm_options[esod_allow_cancellation]' <?php checked( $this->esod_options['esod_allow_cancellation'], 'allow' ); ?> value='allow' id="esod-allow-cancellation">
			<label for="esod-allow-cancellation"><?php _e( 'Let employers cancel shifts.', 'employee-scheduler-on-demand' ); ?></label>
		</p>
		<?php
	}

	public function esod_cancellation_deadline_callback( $args ) {
		?><span class="description"><?php echo $args['description']; ?></span>
		<p>
			<input type="number" step="0.1" name='wpaesm_options[esod_cancellation_deadline]' value='<?php echo $this->esod_options['esod_cancellation_deadline']; ?>' id="esod-cancellation-deadline">
		</p>
		<?php
	}

	public function esod_terms_callback( $args ) {
        ?> <span class="description"><?php echo $args['description']; ?></span>
        <p>
           <select name="wpaesm_options[esod_terms]" id="esod-terms">
	            <option value=""></option>
	            <?php $pages = get_pages( 'post_type=page&posts_per_page=-1&orderby=name&order=ASC');
	            foreach( $pages as $page ) { ?>
	                <option value="<?php echo $page->ID; ?>" <?php selected( $page->ID, $this->esod_options['esod_terms'] ); ?>>
	                    <?php echo $page->post_title; ?>
	                </option>
	            <?php } ?>
	        </select>
        </p><?php
    }


}


