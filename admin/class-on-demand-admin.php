<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://employee-scheduler.co
 * @since      1.0.0
 *
 * @package    On_Demand
 * @subpackage On_Demand/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    On_Demand
 * @subpackage On_Demand/admin
 * @author     Morgan Kay <hello@employee-scheduler.co>
 */
class On_Demand_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->esod_options = get_option( 'wpaesm_options' );

	}

	/**
	 * Register custom post type: Work Criteria Form (work_criteria_form)
     */
	public function register_cpt_on_demand_form() {

		$labels = array(
				'name'                  => _x( 'Work Criteria Forms', 'Post Type General Name', 'employee-scheduler-on-demand' ),
				'singular_name'         => _x( 'Work Criteria Form', 'Post Type Singular Name', 'employee-scheduler-on-demand' ),
				'menu_name'             => __( 'Work Criteria Forms', 'employee-scheduler-on-demand' ),
				'name_admin_bar'        => __( 'Work Criteria Form', 'employee-scheduler-on-demand' ),
				'parent_item_colon'     => __( 'Parent Work Criteria Form:', 'employee-scheduler-on-demand' ),
				'all_items'             => __( 'All Work Criteria Forms', 'employee-scheduler-on-demand' ),
				'add_new_item'          => __( 'Add New Work Criteria Form', 'employee-scheduler-on-demand' ),
				'add_new'               => __( 'Add New', 'employee-scheduler-on-demand' ),
				'new_item'              => __( 'New Work Criteria Form', 'employee-scheduler-on-demand' ),
				'edit_item'             => __( 'Edit Work Criteria Form', 'employee-scheduler-on-demand' ),
				'update_item'           => __( 'Update Work Criteria Form', 'employee-scheduler-on-demand' ),
				'view_item'             => __( 'View Work Criteria Form', 'employee-scheduler-on-demand' ),
				'search_items'          => __( 'Search Work Criteria Form', 'employee-scheduler-on-demand' ),
				'not_found'             => __( 'Not found', 'employee-scheduler-on-demand' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'employee-scheduler-on-demand' ),
				'items_list'            => __( 'Work Criteria Forms list', 'employee-scheduler-on-demand' ),
				'items_list_navigation' => __( 'Work Criteria Forms list navigation', 'employee-scheduler-on-demand' ),
				'filter_items_list'     => __( 'Filter Work Criteria Forms list', 'employee-scheduler-on-demand' ),
		);
		$args = array(
				'label'                 => __( 'Work Criteria Form', 'employee-scheduler-on-demand' ),
				'description'           => __( 'Forms for Employee Scheduler On Demand', 'employee-scheduler-on-demand' ),
				'labels'                => $labels,
				'supports'              => array( 'title' ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => 'users.php',
				'menu_position'         => 5,
				'show_in_admin_bar'     => false,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => false,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capability_type'       => 'page',
		);
		register_post_type( 'work_criteria_form', $args );

	}


	/**
	 * Create WP Alchemy metabox for work criteria form fields
     */
	public function create_metabox() {
		if( class_exists( 'WPAlchemy_MetaBox' ) ) {
			global $work_criteria_form_metabox;
			$work_criteria_form_metabox = new WPAlchemy_MetaBox(array
			(
				'id' => 'work_criteria_meta',
				'title' => 'Form Fields',
				'types' => array( 'work_criteria_form' ),
				'template' => plugin_dir_path( dirname( __FILE__ ) ) . '/admin/partials/work-criteria-form-meta.php',
				'mode' => WPALCHEMY_MODE_EXTRACT,
				'prefix' => '_esod_',
				'context' => 'normal',
				'priority' => 'high',
			));
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/on-demand-admin.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'esod_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}

	public function woocommerce_nag() {
		$user = get_current_user_id();
		$dismissed = get_user_meta( $user, 'esod_dismiss_woocommerce_nag', true );
		if( 'dismissed' == $dismissed ) {
			return;
		}
		?>
		<div class="notice-warning notice is-dismissible esod-wants-woocommerce">
			<p><?php _e( 'Employee Scheduler On Demand can use WooCommerce to automatically create and send invoices to employers.  Install and activate WooCommerce to take advantage of this functionality.  Otherwise, you can create and send invoices manually.', 'employee-scheduler-on-demand' ); ?></p>
		</div>
		<?php
	}

	public function dismiss_woocommerce_nag() {
		$user = get_current_user_id();
		add_user_meta( $user, 'esod_dismiss_woocommerce_nag', 'dismissed' );
	}

	/**
	 * Display work criteria on user profile page
	 *
	 * @param $user
     */
	public function show_criteria_user_meta( $user ) {

		if( !is_admin() ) {
			return;
		}

		echo '<h3>' . __( 'On Demand Work Criteria', 'employee-scheduler-on-demand' ) . '</h3>';

		global $wpdb;
		$meta_fields = $wpdb->get_results( "SELECT meta_key FROM $wpdb->usermeta WHERE meta_key LIKE 'esod_%'", ARRAY_N );
		$meta_fields = array_map( "unserialize", array_unique( array_map( "serialize", $meta_fields ) ) ); // http://stackoverflow.com/a/946300/1112258

		if( empty( $meta_fields ) ) {
			echo '<p>' . __( 'This employee has not entered any work criteria', 'employee-scheduler-on-demand' ) . '</p>';
		} else {
			$forms = $this->make_list_of_forms( $meta_fields );

			foreach( $forms as $form ) { ?>
				<table class="form-table">
					<tbody>
						<tr colspan="2">
							<th>
								<?php echo __( 'Form: ', 'employee-scheduler-on-demand' ) . ucfirst( $form['form_name'] ); ?>
							</th>
						</tr>
						<?php foreach( $meta_fields as $field ) {
							if( $this->starts_with( $field[0], $form['key'] ) ) { ?>
								<tr>
									<th><?php echo $this->pretty_form_field_name( $field[0] ); ?></th>
									<td><?php echo $this->pretty_user_meta( get_user_meta( $user->ID, $field[0], true ) ); ?></td>
								</tr>
							<?php }
						} ?>
					</tbody>
				</table>

			<?php }
		}
	}

	/**
	 * Given a list of the esod user meta fields, generate a list of work criteria forms
	 *
	 * @param $meta_fields
	 *
	 * @return array|string|void
     */
    private function make_list_of_forms( $meta_fields ) {

		$keys = array();

		$i = 0;

		foreach( $meta_fields as $key ) {
			$parts = explode( "_", $key[0] );
			$hyphenated_name = $parts[1];
			$keys[$i]['key'] = 'esod_' . $parts[1];
			$keys[$i]['form_name'] = str_replace( '-', ' ', $hyphenated_name );
			$i++;
		}

		if( !empty( $keys ) ) {
			return array_unique( $keys );
		} else {
			$msg = __( 'Could not find the criteria form', 'employee-scheduler-on-demand' );
			return $msg;
		}
	}

	/**
	 * Check if a user meta key starts with the form key - this tells us if the field belongs to this form
	 *
	 * @see http://stackoverflow.com/a/10473026
	 *
	 * @param $field
	 * @param $form_key
	 *
	 * @return bool
     */
    private function starts_with( $field, $form_key ) {
		return $form_key === "" || strrpos( $field, $form_key, -strlen( $field ) ) !== FALSE;
	}

	/**
	 * Given a user meta field key, return a human-friendly field name
	 *
	 * @param $field_name
	 *
	 * @return mixed
     */
    private function pretty_form_field_name( $field_name ) {
		$parts = explode( "_", $field_name );
		$pretty_name = str_replace( '-', ' ', $parts[2] );
		return $pretty_name;
	}

	/**
	 * Given a user meta field value, return a human-friendly value
	 *
	 * @param $value
	 *
	 * @return mixed|string
     */
    private function pretty_user_meta( $value ) {
		$value = maybe_unserialize( $value );

		if( is_array( $value ) ) {
			$pretty_meta = '<ul>';
			foreach( $value as $item ) {
				$pretty_meta .= '<li>' . str_replace( '-', ' ', $item ) . '</li>';
			}
			$pretty_meta .= '</ul>';
		} else {
			$pretty_meta = str_replace( '-', ' ', $value );
		}

		return $pretty_meta;
	}

	/**
	 * Use P2P to connect employers to shifts
     */
	public function connect_employers_to_shifts() {
		p2p_register_connection_type( array(
			'name' => 'shifts_to_employers',
			'from' => 'shift',
			'to' => 'user',
			'to_query_vars' => array( 'role' => 'employer' ),
			'cardinality' => 'many-to-one',
			'to_labels' => array(
					'singular_name' => __( 'Employer', 'wpaesm' ),
					'search_items' => __( 'Search employers', 'wpaesm' ),
					'not_found' => __( 'No employers found.', 'wpaesm' ),
					'create' => __( 'Add Employer', 'wpaesm' ),
			),
		) );

	}

	/**
	 * When an employee clocks out, create an order in WooCommerce
	 *
	 * @param $shift
	 */
	public function create_order( $shift, $employee='' ) {

		// get employer
		$users = get_users( array(
			'connected_type' => 'shifts_to_employers',
			'connected_items' => $shift
		) );
		if( is_array( $users ) && !empty( $users ) ) {
			$employer = $users[0]->ID;
		} else {
			return;  // no employer, so let's bail here
		}

		// calculate order total
		$total = $this->calculate_order_total( $shift );

		// create order
		$args = array(
			'customer_id'   => $employer,
			'created_via'   => 'Employee Scheduler On Demand',
		);
		$order = wc_create_order( $args );
		$order->set_total( $total );

		add_post_meta( $shift, '_wpaesm_invoice', $order );

		do_action( 'esod-after-create-order', $shift, $order );

	}


	/**
	 * Calculate the total cost of a shift
	 *
	 * @param $shift
	 *
	 * @return float
	 */
	private function calculate_order_total( $shift ) {

		$hours = $this->get_shift_billable_hours( $shift );

		$multiplier = $this->get_hours_multiplier( $shift );

		$total = floatval( $hours ) * floatval( $multiplier );

		return $total;

	}

	/**
	 * Calculate how many billable hours a shift has
	 *
	 * @param $shift
	 *
	 * @return float
	 */
	private function get_shift_billable_hours( $shift ) {

		if( ( isset( $this->esod_options['esod_calc'] ) && 'requested' == $this->esod_options['esod_calc'] ) ||
		!isset( $this->esod_options['esod_calc'] ) ||
		'' == $this->esod_options['esod_calc'] ) {
			// bill for the scheduled time
			// @todo - what happens if start time and end time aren't there or don't make sense?
			$start = get_post_meta( $shift, '_wpaesm_starttime', true );
			$end = get_post_meta( $shift, '_wpaesm_endtime', true );

		} else {
			// bill for the time actually worked
			$start = get_post_meta( $shift, '_wpaesm_clockin', true );
			if( !isset( $start ) || '' == $start ) {
				$start = get_post_meta( $shift, '_wpaesm_starttime', true );
			}
			$end = get_post_meta( $shift, '_wpaesm_clockout', true );
			if( !isset( $end ) || '' == $end ) {
				$end = get_post_meta( $shift, '_wpaesm_endtime', true );
			}

		}

		$to_time = strtotime( $start );
		$from_time = strtotime( $end );

		$minutes = round( abs( $to_time - $from_time ) / 60,2 );
		$quarters = round( $minutes/15 ) * 15;
		$hours   = $quarters / 60;

		return $hours;

	}

	/**
	 * Calculate the hourly cost for a shift
	 *
	 * @param $shift
	 *
	 * @return float|int
	 */
	private function get_hours_multiplier( $shift ) {

		if( ( isset( $this->esod_options['esod_billing_method'] ) && 'flat_hourly' == $this->esod_options['esod_billing_method'] )
		|| !isset( $this->esod_options['esod_billing_method'] ) ) {
			if( isset( $this->esod_options['esod_hourly_rate'] ) && '' !== $this->esod_options['esod_hourly_rate'] ) {
				$multiplier = floatval( $this->esod_options['esod_hourly_rate'] );
				return $multiplier;
			} else {
				// I'm inventing an hourly rate
				return 20;
			}
		} else {
			if( isset( $this->esod_options['esod_markup'] ) && '' !== $this->esod_options['esod_markup'] ) {
				$markup = floatval( $this->esod_options['esod_markup'] );
			} else {
				$markup = 25;
			}

			$users = get_users( array(
				'connected_type' => 'shifts_to_employers',
				'connected_items' => $shift
			) );
			if( is_array( $users ) && !empty( $users ) ) {
				// @todo - failure mode if there is no employee?
				$employee = $users[0]->ID;
			}
			$rate = get_user_meta( $employee, 'wage', true);

			if( !isset( $rate ) || '' == $rate ) {
				$rate = 20;
			}

			$multiplier = floatval( $markup ) * floatval( $rate );
			return $multiplier;
		}

	}

	/**
	 * Notify qualified employees of an available shift
	 *
	 * @param $shift_time
	 * @param $shift
	 */
	public function send_shift_notification_email( $shift_time, $shift ) {

		foreach( $shift_time['results']['available_employees'] as $employee_id ) {
			$from = wpaesm_email_from();

			$employee = get_user_by( 'id', $employee_id );

			$to = $employee->user_email;

			$cc = '';

			$subject = __( 'New Shift Available', 'employee-scheduler-on-demand' );

			$message_text =
				'<p>' . __( 'You are qualified for a requested shift!', 'employee-scheduler-on-demand' ) . '</p>
				<p><strong><a href="' . get_the_permalink( $shift ) . '">' . __( 'Claim this shift', 'employee-scheduler-on-demand' ) . '</a></strong></p>
				<p>' . __( 'More information about the shift: ', 'employee-scheduler-on-demand' ) . '</p>' .
				get_post_field( 'post_content', $shift ) . '
				<p><strong><a href="' . get_the_permalink( $shift ) . '">' . __( 'Claim this shift', 'employee-scheduler-on-demand' ) . '</a></strong></p>';

			wpaesm_send_email( $from, $to, $cc, $subject, $message_text );
		}


	}

	/**
	 * Notify site admin that an employer has requested a shift
	 *
	 * @param $created_shifts
	 */
	public function notify_admins_created_shifts( $created_shifts ) {

		$from = wpaesm_email_from();

		$to = wpaesm_email_from();

		$cc = '';

		$subject = __( 'An Employer Has Created New Shifts', 'employee-scheduler-on-demand' );

		$message_text =
			'<p>' . __( 'An employer has used Employee Scheduler On Demand to request new shifts on your website!  Employees have been notified.  Here are the shifts that were created: ', 'employee-scheduler-on-demand' ) . '</p>
			<ul>';
			foreach( $created_shifts as $shift ) {
				$link = get_the_permalink( $shift );
				$title = get_the_title( $shift );
				$message_text .=
					'<li><a href="' . $link . '">' . $title . '</a></li>';
			}

			$message_text .= '</ul>';

		wpaesm_send_email( $from, $to, $cc, $subject, $message_text );

	}

	/**
	 * Send email notification of invoice to employer
	 *
	 * @param $shift
	 * @param $order
	 */
	public function send_invoice_to_employer( $shift, $order ) {

		$from = wpaesm_email_from();

		$users = get_users( array(
			'connected_type' => 'shifts_to_employers',
			'connected_items' => $shift
		) );
		if( is_array( $users ) && !empty( $users ) ) {
			$to = $users[0]->user_email;
		} else {
			return;
		}

		$cc = '';

		$subject = sprintf( __( 'New invoice from %s', 'employee-scheduler-on-demand' ), get_bloginfo( 'name' ) );

		// @todo - should do some validation here in case values aren't what we expect
		$date_and_time = get_post_meta( $shift, '_wpaesm_date', true ) . ', ' . get_post_meta( $shift, '_wpaesm_starttime', true ) . ' - ' . get_post_meta( $shift, '_wpaesm_endtime', true );
		$message_text = '<p>' . sprintf( __( 'Here is the invoice for the shift you requested for %s:', 'employee-scheduler-on-demand' ), $date_and_time ) . '</p>';
		$message_text .= '<p><a href="' . $order->get_checkout_payment_url() . '">' . $order->get_checkout_payment_url() . '</a></p>';

		wpaesm_send_email( $from, $to, $cc, $subject, $message_text );

	}

	/**
	 * Add more fields to the shift metabox
	 *
	 * @param $metabox
	 */
	public function shift_metabox_fields( $metabox ) { ?>
		<tr>
			<th scope="row"><label><?php _e( 'Invoice', 'employee-scheduler-on-demand' ); ?></label></th>
			<td>
				<?php $metabox->the_field('invoice'); ?>
				<span><?php _e('If you are using WooCommerce, the order ID will be automatically entered here.  Otherwise, you can use this field to enter a link to your invoice.', 'employee-scheduler-on-demand'); ?></span><br />
				<input type="text" name="<?php $metabox->the_name(); ?>" value="<?php echo $metabox->get_the_value(); ?>"/>
			</td>
		</tr>

	<?php }

	/**
	 * Notify employer when an employee has accepted a shift
	 *
	 * @param $shift
	 * @param $employee
	 */
	public function notify_employer_shift_taken( $shift, $employee ) {

		$from = wpaesm_email_from();

		$users = get_users( array(
			'connected_type' => 'shifts_to_employers',
			'connected_items' => $shift
		) );
		if( is_array( $users ) && !empty( $users ) ) {
			$to = $users[0]->user_email;
		} else {
			return;
		}

		$cc = '';

		$subject = __( 'Your Shift Request Has Been Accepted!', 'employee-scheduler-on-demand' );

		$date_and_time = get_post_meta( $shift, '_wpaesm_date', true ) . ', ' . get_post_meta( $shift, '_wpaesm_starttime', true ) . ' - ' . get_post_meta( $shift, '_wpaesm_endtime', true );
		$message_text = sprintf( __( 'Good news! %s is going to work the shift you requested for %s.', 'employee-scheduler-on-demand' ), $employee->display_name, $date_and_time );

		wpaesm_send_email( $from, $to, $cc, $subject, $message_text );
	}
}
