<?php
/*
 * Metabox used to create work criteria forms
 */
?>
<div class="my_meta_control" id="work_criteria_form">

	<a href="#" class="dodelete-question button"><?php _e('Remove All Criteria', 'employee-scheduler-on-demand'); ?></a>

<?php while($mb->have_fields_and_multi('question')): ?>
    <?php $mb->the_group_open(); ?>

    <div class="question location clearfix">

        <?php $mb->the_field('name'); ?>
        <label><?php _e( 'Criterion Name', 'employee-scheduler-on-demand' ); ?></label>
        <p class="description" style="display:block;"><?php _e( 'Required.  1-3 word name of the criterion.  Alphanumeric characters only.', 'employee-scheduler-on-demand' ); ?></p>
        <p><input type="text" class="widefat" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>

        <?php $mb->the_field('description'); ?>
        <label><?php _e( 'Criterion Description', 'employee-scheduler-on-demand' ); ?></label>
        <p class="description" style="display:block;"><?php _e( 'Optional.  Longer description/explanation of the field.', 'employee-scheduler-on-demand' ); ?></p>
        <p><input type="text" class="widefat" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>


        <div class="half left">
            <?php $mb->the_field('format'); ?>
            <label><?php _e('Format', 'employee-scheduler-on-demand'); ?></label>
            <?php $selected = ' selected="selected"'; ?>
            <select name="<?php $mb->the_name(); ?>" class="format-select">
                <option value=""></option>
<!--                <option value="text" --><?php //if ($mb->get_the_value() == "text") echo $selected; ?><!-->--><?php //_e( 'One-line text', 'employee-scheduler-on-demand'); ?><!--</option>-->
                <option value="select" <?php if ($mb->get_the_value() == "select") echo $selected; ?>><?php _e( 'Drop-down select', 'employee-scheduler-on-demand'); ?></option>
                <option value="checkbox" <?php if ($mb->get_the_value() == "checkbox") echo $selected; ?>><?php _e( 'Checkbox(es)', 'employee-scheduler-on-demand'); ?></option>
                <option value="radio" <?php if ($mb->get_the_value() == "radio") echo $selected; ?>><?php _e( 'Radio buttons', 'employee-scheduler-on-demand'); ?></option>
            </select>

            <?php $mb->the_field('required'); ?>
            <label><?php _e('Required', 'employee-scheduler-on-demand'); ?></label>
            <p><input type="checkbox" name="<?php $mb->the_name(); ?>" value="1"<?php if ($mb->get_the_value()) echo ' checked="checked"'; ?>/> <?php _e('This question is required', 'employee-scheduler-on-demand'); ?></p>

            <?php $mb->the_field('order'); ?>
            <label><?php _e('Order', 'employee-scheduler-on-demand'); ?></label>
            <p><input type="number" min="0" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>

        </div>


        <div class="half right">
            <?php
            $mb->the_field('format');
            $display = 'none';
            if( "select" == $mb->get_the_value() || "checkbox" == $mb->get_the_value() || "radio" == $mb->get_the_value() ) {
                $display = 'block';
            } ?>

            <fieldset class="half right option-fieldset" style="display: <?php echo $display; ?>;">
                <legend><?php _e('Options', 'employee-scheduler-on-demand'); ?></legend>

                <a href="#" class="dodelete-option button"><?php _e( 'Remove All Options', 'employee-scheduler-on-demand' ); ?></a>
<?php // @todo - make nicer UI with plus minus icons ?>
                <?php while($mb->have_fields_and_multi('option')): ?>
                    <?php $mb->the_group_open(); ?>
                    <?php $mb->the_field('option'); ?>
                    <label><?php _e('Option', 'employee-scheduler-on-demand'); ?></label>
                    <p><input type="text" class="option" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>

                    <a href="#" class="dodelete button"><?php _e('Remove This Option', 'employee-scheduler-on-demand'); ?></a>
                    <hr />

                    <?php $mb->the_group_close(); ?>
                <?php endwhile; ?>
                <p><a href="#" class="docopy-option button"><?php _e('Add Another Option', 'employee-scheduler-on-demand'); ?></a></p>
            </fieldset>
        </div>



        <p class="clearfix"><a href="#" class="dodelete button"><?php _e('Remove This Criterion', 'employee-scheduler-on-demand'); ?></a></p>

    </div>
    <hr />
    <?php $mb->the_group_close(); ?>
<?php endwhile; ?>
<p><a href="#" class="docopy-question button"><?php _e('Add Another Criterion', 'employee-scheduler-on-demand'); ?></a></p>


</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on( 'change', '.format-select', function () {
            var val = $(this).val();
            var fields = $(this).closest('.question').children('.right').children('.option-fieldset');
            if (val === 'checkbox' || val === 'select' || val === 'radio' ) {
                fields.show('slow');
            } else if (val === '' || val === 'text' || val === 'textarea' ) {
                fields.hide('slow');
            }
        });

    });
</script>