(function( $ ) {
	'use strict';

	$( window ).load(function() {

		jQuery(document).on( 'click', '.esod-wants-woocommerce .notice-dismiss', function() {

			jQuery.ajax({
				url: esod_ajax.ajaxurl,
				data: {
					action: 'dismiss_woocommerce_nag'
				}
			})

		})
	});

})( jQuery );
