<?php
/**
 * Display the employer_account shortcode
 */
?>

<div id="esod-employer-account">
    <table>
        <thead>
            <th><?php _e( 'Date and Time', 'employee-scheduler-on-demand' ); ?></th>
            <th><?php _e( 'Employee', 'employee-scheduler-on-demand' ); ?></th>
            <th><?php _e( 'Invoice', 'employee-scheduler-on-demand' ); ?></th>
            <th><?php _e( 'Actions', 'employee-scheduler-on-demand' ); ?></th>
        </thead>
        <?php while ( $shifts->have_posts() ) : $shifts->the_post();
            $date = get_post_meta( get_the_id(), '_wpaesm_date', true );
            $time = get_post_meta( get_the_id(), '_wpaesm_starttime', true ) . ' - ' . get_post_meta( get_the_id(), '_wpaesm_endtime', true );
            $employees = get_users( array(
                'connected_type' => 'shifts_to_employees',
                'connected_items' => get_the_id(),
                ) );
            if( isset( $employees[0] ) ) {
                $employee = $employees[0]->display_name;
            } else {
                $employee = __( 'No one has claimed this shift yet', 'employee-scheduler-on-demand' );
            }

            if( has_term( 'cancelled', 'shift_status', get_the_id() ) ) {
                $employee = __( 'This shift has been cancelled', 'employee-scheduler-on-demand' );
            }

            $invoice = get_post_meta( get_the_id(), '_wpaesm_invoice', true );
            if( isset( $invoice ) && '' !==( $invoice ) ) {
                if( is_numeric( $invoice ) ) {
                    $order = new WC_Order( $invoice );
                    $invoiceurl = $order->get_checkout_payment_url();
                    // @todo - echo currency
                    $invoicelink = '<a href="' . $invoiceurl . '">' . $order->get_total() . ', ' . $order->get_status() . '</a>';
                } elseif( TRUE === filter_var( $invoice, FILTER_VALIDATE_URL ) ) {
                    $invoicelink = '<a href="' . $invoice . '">' . __( 'View Invoice', 'employee-scheduler-on-demand' ) . '</a>';
                } else {
                    $invoicelink = __( 'No Invoice Found', 'employee-schedule-manager' );
                }
            }
             ?>

            <tr>
                <td>
                    <?php echo $date . ', ' . $time; ?>
                </td>
                <td>
                    <?php echo $employee; ?>
                </td>
                <td>
                    <?php echo $invoicelink; ?>
                </td>
                <td>
                    <a class="button" href="<?php the_permalink(); ?>"><?php _e( 'View Shift Details', 'employee-scheduler-on-demand' ); ?></a>
                    <?php if( $this->request_can_be_cancelled( get_the_id() ) ) { ?>
                        <form class="esod-cancel-shift" method="post" action="<?php echo $page_id; ?>">
                            <input type="hidden" class="esod-cancelled-shift-id" name="esod_cancelled_shift_id" value="<?php echo get_the_id(); ?>">
                            <?php wp_nonce_field( 'esod-cancel-shift-request', 'esod-cancel-shift-nonce' ); ?>
                            <input type="submit" value="<?php _e( 'Cancel Shift', 'employee-scheduler-on-demand' ); ?>">
                        </form>
                    <?php } ?>
                </td>
            </tr>
            <?php unset( $invoicelink ); ?>
        <?php endwhile; ?>
    </table>
</div>