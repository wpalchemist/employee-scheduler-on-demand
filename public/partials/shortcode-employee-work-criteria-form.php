<?php
/**
 * Display the employee-work-criteria-form shortcode
 */
?>

<div id="esod-employee-work-criteria">

    <?php if( isset( $_POST['employee-work-criteria'] ) && 'Submit' == $_POST['employee-work-criteria'] ) { ?>
        <p class="esod-success"><?php _e( 'Your information was saved successfully', 'employee-scheduler-on-demand' ); ?></p>
    <?php } ?>

    <form id="employee-work-criteria" method="post" action="<?php get_the_permalink(); ?>">
        <?php foreach( $meta as $field ) {
            echo $this->output_field( $field, $form );
        }
        wp_nonce_field( 'esod-employee-work-criteria', 'esod-employee-nonce' );
        ?>
        <input type="hidden" name="form_slug" value="<?php echo $form; ?>">
        <input type="hidden" name="user_id" value="<?php echo $user->ID ?>">
        <input type="submit" value="<?php _e( 'Submit', 'employee-scheduler-on-demand' ); ?>" name="employee-work-criteria">
    </form>
</div>