<?php
/**
 * In employer-shift-request-form shortcode, if the employer has filled out the form, ask them to confirm shifts
 */
?>

<div id="esod-employer-request-confirmation">
	<h3><?php _e( 'Confirm Your Shift Request', 'employee-scheduler-on-demand' ); ?></h3>
	<form id="employer-request-confirmation" method="post" action="<?php get_the_permalink(); ?>">
		<ul>

			<?php foreach( $_POST['when'] as $shift ) { ?>
				<li>
					<strong><?php _e( 'Requested shift: ', 'employee-scheduler-on-demand' );
						echo $shift['day'] . ', ' . $shift['start'] . ' - ' . $shift['end']; ?>
					</strong><br />
					<?php echo $shift['results']['msg']; ?>
				</li>
			<?php } ?>

		</ul>
		<?php wp_nonce_field( 'esod-employer-shift-request-confirmation', 'esod-confirmation-nonce' ); ?>

		<?php $shifts = base64_encode( serialize( $_POST ) ); ?>

		<input type="hidden" name="esod-request-details" value='<?php echo $shifts; ?>'>
		<input type="hidden" name="esod-employer" value="<?php echo $user->ID; ?>">

		<input type="submit" name="esod-shift-request-confirm" value="<?php _e( 'Confirm Shift Request', 'employee-scheduler-on-demand' ); ?>" />
		<input type="submit" name="esod-shift-request-cancel" value="<?php _e( 'Cancel Shift Request and Enter Different Criteria', 'employee-scheduler-on-demand' ); ?>" />

	</form>
</div>