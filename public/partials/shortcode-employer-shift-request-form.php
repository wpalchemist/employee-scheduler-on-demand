<?php
/**
 * Display the employer-shift-request-form shortcode
 */
?>

<div id="esod-employer-work-request">
    <form id="employer-work-request" method="post" action="<?php get_the_permalink(); ?>">
        <?php
        if( isset( $meta ) && is_array( $meta ) ) { ?>
            <h3><?php _e( 'Find An Employee Who Matches These Criteria:', 'employee-scheduler-on-demand' ); ?></h3>
            <?php foreach( $meta as $field ) {
                echo $this->output_field( $field, $form );
            }
        } ?>
        <h3><?php _e( 'To Work On:', 'employee-scheduler-on-demand' ); ?></h3>
        <script type="text/template" id="availability-template">
            <p class="repeating-availability clearfix">
                <span class="esod-fourth">
                    <label for="when[0][day]"><?php _e( 'Date', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                    <input type="text" name="when[0][day]" id="when[0][day]" class="esod-date" required />
                </span>
                <span class="esod-fourth">
                    <label for="when[0][start]"><?php _e( 'Start Time', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                    <input type="text" name="when[0][start]" id="when[0][start]" class="starttime" required />
                </span>
                <span class="esod-fourth">
                    <label for="when[0][end]"><?php _e( 'End Time', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                    <input type="text" name="when[0][end]" id="when[0][end]" class="endtime" required />
                </span>
                <a href="#" class="remove-availability esod-fourth"><?php _e( 'Remove', 'employee-scheduler-on-demand' ); ?></a>
            </p>
        </script>
        <p class="repeating-availability clearfix">
            <span class="esod-fourth">
                <label for="when[0][day]"><?php _e( 'Date', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                <input type="text" name="when[0][day]" id="when[0][day]" class="esod-date" required />
            </span>
            <span class="esod-fourth">
                <label for="when[0][start]"><?php _e( 'Start Time', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                <input type="text" name="when[0][start]" id="when[0][start]" class="starttime" required />
            </span>
            <span class="esod-fourth">
                <label for="when[0][end]"><?php _e( 'End Time', 'employee-scheduler-on-demand' ); ?>:&nbsp</label><br />
                <input type="text" name="when[0][end]" id="when[0][end]" class="endtime" required />
            </span>
            <a href="#" class="remove-availability esod-fourth"><?php _e( 'Remove', 'employee-scheduler-on-demand' ); ?></a>
        </p>
        <p><a href="#" class="repeat-availability"><?php _e( 'Add Another', 'employee-scheduler-on-demand' ); ?></a></p>

        <p class="esod-same-employee" style="display:none;">
            <label for="same-employee" class="esod-field-label"><?php _e( 'Same employee for all shifts?', 'employee-scheduler-on-demand' ); ?></label>
            <span class="esod-field-description"><?php _e( 'Do you want the same employee to work all shifts, or can different employees take different shifts?', 'employee-scheduler-on-demand' ); ?></span>
            <input type="checkbox" name="same-employee" id="same-employee" value="same-employee" />
            <label for="same-employee"><?php _e( 'I want one employee to work all my shifts.', 'employee-scheduler-on-demand' ); ?></label>

        </p>

        <p>
            <label for="location" class="esod-field-label"><?php _e( 'Location', 'employee-scheduler-on-demand' ); ?></label>
            <span class="esod-field-description"><?php _e( 'Enter the address where the work will be done, or leave blank if location does not matter.', 'employee-scheduler-on-demand' ); ?></span>
            <textarea name="location" id="location"></textarea>
        </p>

        <p>
            <label for="message" class="esod-field-label"><?php _e( 'Description or Comments', 'employee-scheduler-on-demand' ); ?></label>
            <span class="esod-field-description"><?php _e( 'Provide any other necessary information about the work you need done.', 'employee-scheduler-on-demand' ); ?></span>
            <textarea name="message" id="message"></textarea>
        </p>

        <?php if( isset( $this->esod_options['esod_terms'] ) && '' !== $this->esod_options['esod_terms'] && 0 !== $this->esod_options['esod_terms'] ) {
            $link = get_the_permalink( $this->esod_options['esod_terms'] ); ?>
            <p>
                <input type="checkbox" name="terms" id="terms" value="agree-to-terms" required />
                <label for="terms"><?php printf( __( 'I agree to the <a href="%s" target="_blank">terms and conditions</a>', 'employee-scheduler-on-demand' ), esc_url( $link ) ); ?></label>

            </p>
        <?php } ?>

        <?php wp_nonce_field( 'esod-employer-shift-request', 'esod-employer-nonce' ); ?>
        <?php if( '' !== $form ) { ?>
            <input type="hidden" name="form_slug" value="<?php echo $form; ?>">
        <?php } ?>
        <input type="hidden" name="user_id" value="<?php echo $user->ID ?>">
        <p><input type="submit" value="<?php _e( 'Submit', 'employee-scheduler-on-demand' ); ?>" name="employer-shift-request" id="employer-shift-request"></p>
    </form>

    <div id="esod-loading" style="display:none">
        <p><?php _e( 'Searching for employees....', 'employee-scheduler-on-demand' ); ?></p>
    </div>
</div>