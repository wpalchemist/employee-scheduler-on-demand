<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://employee-scheduler.co
 * @since      1.0.0
 *
 * @package    On_Demand
 * @subpackage On_Demand/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    On_Demand
 * @subpackage On_Demand/public
 * @author     Morgan Kay <hello@employee-scheduler.co>
 */
class On_Demand_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->esod_options = get_option( 'wpaesm_options' );

	}

	/**
	 * Display an error message and login form
	 *
	 * @return string
     */
	public function require_login( $role = 'employee' ) {
		$login = '<p>' . __( 'You must be logged in to view this page.', 'employee-scheduler-on-demand' ) . '</p>';
		$login .= '<h3>' . __( 'Existing Users: Log In', 'employee-scheduler-on-demand' ) . '</h3>';
		$args = array(
				'echo' => false,
		);
		$login .= wp_login_form( $args );
		$login .= '<p><a href="' . esc_url( wp_lostpassword_url( get_permalink() ) ) . '">' . __( 'Lost Password', 'employee-scheduler-on-demand' ) . '</a>';

		if( 'employer' == $role ) {
			$login .= $this->employer_registration();
		}
		return $login;
	}

	/**
	 * Display a registration form for employers
	 *
	 * @return string
     */
	public function employer_registration() {

		if( !empty( $_POST ) ) {
			$msg = '<p class="esod-registration-message">' . $this->create_new_user() . '</p>';
			return $msg;
		}
		$registration = '<h3>' . __( 'New Users: Register for an Account', 'employee-scheduler-on-demand' ) . '</h3>';
		$registration .= $this->display_registration_form();
		return $registration;
	}

	/**
	 * Display the employer registration form
	 *
	 * @return string
     */
	public function display_registration_form() {
		$form = '
			<form name="esod-employee-registration" action="' . esc_url( get_the_permalink() ) . '" method="post">
				<p>
			        <label for="first_name">' . __( 'First Name', 'employee-scheduler-on-demand' ) . '</label>
			        <input type="text" name="first_name" value="" required>
			    </p>
			    <p>
			        <label for="last_name">' . __( 'Last Name', 'employee-scheduler-on-demand' ) . '</label>
			        <input type="text" name="last_name" value="" required>
			    </p>
			    <p>
			        <label for="user_login">' . __( 'Username', 'employee-scheduler-on-demand' ) . '</label>
			        <input type="text" name="user_login" value="" required>
			    </p>
			    <p>
			        <label for="user_email">' . __( 'E-mail', 'employee-scheduler-on-demand' ) . '</label>
			        <input type="email" name="user_email" id="user_email" value="" required>
			    </p>
			    <p style="display:none">
			        <label for="confirm_email">Please leave this field empty</label>
			        <input type="text" name="confirm_email" id="confirm_email" value="">
			    </p>

			    <p id="reg_passmail">A password will be e-mailed to you.</p>'

		        . wp_nonce_field( 'esod-employer-registration', 'esod-registration-nonce' ) .

			    '<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" value="' . __( 'Register', 'employee-scheduler-on-demand' ) . '" /></p>
			</form>';

		return $form;
	}

	/**
	 * Create new employer user
	 *
	 * @return array|bool|mixed
     */
	private function create_new_user() {
		// verify nonce
		$nonce = $_POST['esod-registration-nonce'];
		if ( !wp_verify_nonce( $nonce, 'esod-employer-registration'  ) ) {
			wp_die( 'Permission Error' );
		}

		if ( isset( $_POST['confirm_email'] ) && '' !== $_POST['confirm_email'] ) {
			wp_die( 'Are you human?' );
		}

		// check that all the fields are filled out and validate them
		if( !isset( $_POST['first_name'] ) || '' == $_POST['first_name'] ) {
			$msg = __( 'You must enter your first name', 'employee-scheduler-on-demand' );
		} else {
			$first_name = sanitize_text_field( $_POST['first_name'] );
		}
		if( !isset( $_POST['last_name'] ) || '' == $_POST['last_name'] ) {
			$msg = __( 'You must enter your last name', 'employee-scheduler-on-demand' );
		} else {
			$last_name = sanitize_text_field( $_POST['last_name'] );
		}
		if( !isset( $_POST['user_email'] ) || '' == $_POST['user_email'] ) {
			$msg = __( 'You must enter your email', 'employee-scheduler-on-demand' );
		} else {
			$user_email = sanitize_email( $_POST['user_email'] );
		}
		if( !isset( $_POST['user_login'] ) || '' == $_POST['user_login'] ) {
			$msg = __( 'You must enter your login name', 'employee-scheduler-on-demand' );
		} else {
			$user_login = sanitize_text_field( $_POST['user_login'] );
		}

		if( isset( $msg ) ) {
			return $msg;
		}

		$userdata = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'user_email' => $user_email,
			'user_login' => $user_login,
			'role' => 'employer',
		);

		// insert the user
		$user_id = wp_insert_user( $userdata );

		if ( is_wp_error( $user_id ) ) {
			$msg = __( 'There was an error creating your account.  Please go back and try again.', 'employee-scheduler-on-demand' );
		} else {
			wp_new_user_notification( $user_id, '', 'both' );
			$msg =  __( 'Your account was created successfully! Check your email for a link to set your password, and the login with the form above.', 'employee-scheduler-on-demand' );
		}

		return $msg;
	}

	/**
	 * Check whether a user has a certain role
	 *
	 * @param $role
	 * @param null $user_id
	 *
	 * @return bool
     */
    public function check_user_role( $role, $user_id = null ) {

		if ( is_numeric( $user_id ) )
			$user = get_userdata( $user_id );
		else
			$user = wp_get_current_user();

		if ( empty( $user ) )
			return false;

		return in_array( $role, (array) $user->roles );
	}


	/**
	 * Shortcode to display employee work criteria form
	 *
	 * Employees can fill out this form to save/edit their work criteria
	 *
	 * @param $atts  'Form' is a required attribute - the slug of the form
	 *
	 * @return string|void
     */
    public function employee_work_criteria_form_shortcode( $atts ) {

		// Attributes
		extract( shortcode_atts(
			array(
					'form' => '',
			), $atts )
		);

		wp_enqueue_style( 'employee-scheduler', plugins_url( 'employee-scheduler/css/employee-scheduler.css' ) );

		if( '' == $form ) {
			// @todo - link to appropriate documentation
			$output = __( 'You must select a form to display.  See plugin documentation for more details.', 'employee-scheduler-on-demand' );
			return $output;
		}

		if( isset( $_POST['employee-work-criteria'] ) && 'Submit' == $_POST['employee-work-criteria'] ) {
			$this->save_employee_work_criteria();
		}

		$meta = $this->get_form_meta( $form );

		if( is_array( $meta ) ) { // we got an array value, so there must be form fields to display
			if( is_user_logged_in() && ( $this->check_user_role( 'employee' ) || $this->check_user_role( 'administrator' ) ) ) {
				$user = wp_get_current_user();
				ob_start();

				include 'partials/shortcode-employee-work-criteria-form.php';

				return ob_get_clean();
			} else {
				return $this->require_login();
			}
		} else {
			return $meta; // we didn't get an array, so we must have an error message to display
		}

	}

	/**
	 * Search for a work criteria form, return the meta data
	 *
	 * @param $form_slug
	 *
	 * @return string|void
     */
    public function get_form_meta( $form_slug ) {

		$args = array(
				'name' => $form_slug,
			    'post_type' => 'work_criteria_form',
                'posts_per_page' => 1,
		);

		$form_query = new WP_Query( $args );

		// The Loop
		if ( $form_query->have_posts() ) {
			while ( $form_query->have_posts() ) : $form_query->the_post();
				global $work_criteria_form_metabox;
				$form_meta = $work_criteria_form_metabox->the_meta();
				$form_fields = $form_meta['question'];
				if( is_array( $form_fields) ) {
					wp_reset_postdata();
					usort( $form_fields, array( $this, 'sort_order' ) );
					return $form_fields;
				} else {
					wp_reset_postdata();
					$msg = __( 'This employee work criteria form does not have any fields.', 'employee-scheduler-on-demand' );
					return $msg;
				}
			endwhile;
		} else {
			wp_reset_postdata();
			$msg = __( 'No form found.  Check the form name and try again.', 'employee-scheduler-on-demand' );
			return $msg;
		}

	}

	/**
	 * Sort form fields by the user-entered order
	 *
	 * @param $a
	 * @param $b
	 *
	 * @return int
     */
    public function sort_order( $a, $b ) {
		if( $a['order'] == $b['order'] ) {
			return 0;
		}
		return( $a['order'] < $b['order'] ) ? -1 : 1;
	}

	/**
	 * Output form fields
	 *
	 * Process form fields to display in shortcode.
	 *
	 * @see shortcode-employee-work-criteria-form.php
	 *
	 * @param $field
	 *
	 * @return string
     */
    public function output_field( $field, $form_slug ) {

		$no_options_error = '<p>' . __( 'You have not created any options for this field.', 'employee-scheduler-on-demand' ) . '</p>';

	    if( !isset( $field['name'] ) || '' == $field['name'] ) {
		    $errmsg = __( 'You have not set a name for this field.  Please edit the form create a name for this field.', 'employee-scheduler-on-demand' );
		    return $errmsg;
	    }

	    $label = '<p><label class="esod-field-label">' . esc_html( $field['name'] ) .  '</label>';

	    if( isset( $field['description'] ) ) {
		    $label .= '<span class="esod-field-description">' .esc_html( $field['description'] ) . '</span>';
	    }

	    $form_slug = sanitize_text_field( $form_slug );
	    $form_slug = str_replace( '_', '-', $form_slug );
	    $field_name = 'esod_' . $form_slug . '_' . $this->stripped_field_name( $field['name'] );

	    $user = wp_get_current_user();
	    $existing = get_user_meta( $user->ID, 'esod_' . $form_slug . '_' . $this->stripped_field_name( $field['name'] ), true );

	    if( isset( $existing ) && !empty( $existing ) ) {
		    $value = maybe_unserialize( $existing );
	    } else {
		    $value = '';
	    }

		switch( $field['format'] ) {
			case 'text';
				$text = '
						<input
							type="text"
							name="meta[' . $field_name . ']"
							value="' . $value . '">
					</p>';
				return $label . $text;
				break;

			case 'select';
				if( !isset( $field['option'] ) || !is_array( $field['option'] ) ) {
					return $no_options_error;
				}
				$select = '
						<select name="meta[' . $field_name . ']">
						<option value=""> </option>';
						foreach( $field['option'] as $option ) {
							$select .= '<option
								value="' . $this->stripped_field_name( $option['option'] ) . '"
								' . selected( $value, $this->stripped_field_name( $option['option'] ), false ) . '>
									' . esc_attr( $option['option' ] ) . '
								</option>';
						}
						$select .= '</select></p>';
				return $label . $select;
				break;

			case 'checkbox';
				if( !isset( $field['option'] ) ) {
					return $no_options_error;
				}

				$checkbox = '';
				foreach( $field['option'] as $option ) {
					if( is_array( $value ) && in_array( $this->stripped_field_name( $option['option'] ), $value ) ) {
						$checked = 'checked';
					} else {
						$checked = '';
					}
					$checkbox .= '<input
								type="checkbox"
								name="meta[' . $field_name . '][]"
								value="' . $this->stripped_field_name( $option['option'] ) . '"
								id="' . $this->stripped_field_name( $option['option'] ) . '"
								' . $checked . '>&nbsp;
									<label for="' . $this->stripped_field_name( $option['option'] ) . '">
										' . esc_attr( $option['option' ] ) . '
									</label>
								<br />';
				}
				$checkbox .= '</p>';
				return $label . $checkbox;
				break;

			case 'radio';
				if( !isset( $field['option'] ) ) {
					return $no_options_error;
				}
				$radio = '';
				foreach( $field['option'] as $option ) {
					$radio .= '<input
								type="radio"
								name="meta[' . $field_name . ']"
								value="' . $this->stripped_field_name( $option['option'] ) . '"
								id="' . $this->stripped_field_name( $option['option'] ) . '"
								' . checked( $value, $this->stripped_field_name( $option['option'] ), false ) . '>&nbsp;
									<label for="' . $this->stripped_field_name( $option['option'] ) . '">
										' . esc_attr( $option['option' ] ) . '
									</label>
								<br />';
				}
				$radio .= '</select></p>';
				return $label . $radio;
				break;
		}

	    $msg = __( 'No field information found', 'employee-scheduler-on-demand' );
	    return $msg; // just in case we get this far
	}

	/**
	 * Clean up field names for use in HTML
	 *
	 * @param $field_name
	 *
	 * @return mixed
     */
    public function stripped_field_name( $field_name ) {
	    $field_name = sanitize_text_field( $field_name );
		return str_replace( " ", "-", $field_name );
	}

	/**
	 * When an employee fills out the work criteria form, save the data
     */
	private function save_employee_work_criteria() {

		$meta = get_user_meta( $_POST['user_id'] );

		if ( ! wp_verify_nonce( $_POST['esod-employee-nonce'], 'esod-employee-work-criteria' ) ) {
			wp_die( __( 'Permission error', 'employee-scheduler-on-demand' ) );
		}

		$employee = get_user_by( 'id', absint( $_POST['user_id'] ) );

		if( !$employee ) {
			wp_die( __( 'Unable to find your user account.  Please log out and back in.', 'employee-scheduler-on-demand' ) );
		}

		if( !isset( $_POST['meta'] ) || empty( $_POST['meta'] ) ) {
			wp_die( __( 'You did not provide any information.', 'employee-scheduler-on-demand' ) );
		}

		foreach( $_POST['meta'] as $key => $value ) {
			$user_id = absint( $_POST['user_id'] );

			delete_user_meta( $user_id, $key );
			update_user_meta( $user_id, $key, $value, true );
		}

	}

	/**
	 * Display form fields where employees can enter their work availability
	 *
	 * @param $user
     */
	public function employee_availability_fields( $user ) { ?>
		<tr>
			<th><label for="availability"><?php _e( 'Availability', 'employee-scheduler-on-demand' ); ?></label></th>
			<script type="text/template" id="availability-template">
				<div class="repeating-availability">
					<p class="third" style="display: inline-block;">
						<label><?php _e("Day", "employee-scheduler-on-demand"); ?></label><br />
						<select name="availability[0][day]" id="availability[0][day]">
							<option value=""></option>
							<option value="sunday"><?php _e( 'Sunday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="monday"><?php _e( 'Monday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="tuesday"><?php _e( 'Tuesday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="wednesday"><?php _e( 'Wednesday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="thursday"><?php _e( 'Thursday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="friday"><?php _e( 'Friday', 'employee-scheduler-on-demand' ); ?></option>
							<option value="saturday"><?php _e( 'Saturday', 'employee-scheduler-on-demand' ); ?></option>
						</select>
					</p>
					<p class="third" style="display: inline-block;">
						<label><?php _e( "From", "employee-scheduler-on-demand"); ?></label><br />
						<input type="text" name="availability[0][start]" id="availability[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'availability[0][start]', $user->ID ) ); ?>" class="short-text" />
					</p>
					<p class="third" style="display: inline-block;">
						<label><?php _e( "To", "employee-scheduler-on-demand" ); ?></label><br />
						<input type="text" name="availability[0][end]" id="availability[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'availability[0][end]', $user->ID ) ); ?>" class="short-text" />
					</p>
					<a href="#" class="remove-availability"><?php _e( 'Remove', 'employee-scheduler-on-demand'); ?></a>
				</div>
			</script>
			<td>
				<?php
				$availability = get_the_author_meta( 'availability', $user->ID );
				if( is_array( $availability ) ) {
					$i = 0;
					foreach( $availability as $available ) { ?>
						<div class="repeating-availability">
							<p class="third" style="display: inline-block;">
								<label><?php _e("Day", "employee-scheduler-on-demand"); ?></label><br />
								<select name="availability[<?php echo $i; ?>][day]" id="availability[<?php echo $i; ?>][day]">
									<option value=""></option>
									<option value="sunday" <?php selected( $available['day'], 'sunday' ); ?>><?php _e( 'Sunday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="monday" <?php selected( $available['day'], 'monday' ); ?>><?php _e( 'Monday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="tuesday" <?php selected( $available['day'], 'tuesday' ); ?>><?php _e( 'Tuesday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="wednesday" <?php selected( $available['day'], 'wednesday' ); ?>><?php _e( 'Wednesday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="thursday" <?php selected( $available['day'], 'thursday' ); ?>><?php _e( 'Thursday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="friday" <?php selected( $available['day'], 'friday' ); ?>><?php _e( 'Friday', 'employee-scheduler-on-demand' ); ?></option>
									<option value="saturday" <?php selected( $available['day'], 'saturday' ); ?>><?php _e( 'Saturday', 'employee-scheduler-on-demand' ); ?></option>
								</select>
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "From", "employee-scheduler-on-demand"); ?></label><br />
								<input type="text" name="availability[<?php echo $i; ?>][start]" id="availability[<?php echo $i; ?>][start]" class="starttime" value="<?php echo $available['start']; ?>" class="short-text" />
							</p>
							<p class="third" style="display: inline-block;">
								<label><?php _e( "To", "employee-scheduler-on-demand" ); ?></label><br />
								<input type="text" name="availability[<?php echo $i; ?>][end]" id="availability[<?php echo $i; ?>][end]" class="endtime" value="<?php echo $available['end']; ?>" class="short-text" />
							</p>
							<a href="#" class="remove-availability"><?php _e( 'Remove', 'employee-scheduler-on-demand'); ?></a>
						</div>
						<?php $i++;
					}
				}  else { ?>
					<div class="repeating-availability">
						<p class="third" style="display: inline-block;">
							<label><?php _e("Day", "employee-scheduler-on-demand"); ?></label><br />
							<select name="availability[0][day]" id="availability[0][day]">
								<option value=""></option>
								<option value="sunday"><?php _e( 'Sunday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="monday"><?php _e( 'Monday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="tuesday"><?php _e( 'Tuesday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="wednesday"><?php _e( 'Wednesday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="thursday"><?php _e( 'Thursday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="friday"><?php _e( 'Friday', 'employee-scheduler-on-demand' ); ?></option>
								<option value="saturday"><?php _e( 'Saturday', 'employee-scheduler-on-demand' ); ?></option>
							</select>
						</p>
						<p class="third" style="display: inline-block;">
							<label><?php _e( "From", "employee-scheduler-on-demand"); ?></label><br />
							<input type="text" name="availability[0][start]" id="availability[0][start]" class="starttime" value="<?php echo esc_attr( get_the_author_meta( 'availability[0][start]', $user->ID ) ); ?>" class="short-text" />
						</p>
						<p class="third" style="display: inline-block;">
							<label><?php _e( "To", "employee-scheduler-on-demand" ); ?></label><br />
							<input type="text" name="availability[0][end]" id="availability[0][end]" class="endtime" value="<?php echo esc_attr( get_the_author_meta( 'availability[0][end]', $user->ID ) ); ?>" class="short-text" />
						</p>
						<a href="#" class="remove-availability"><?php _e( 'Remove', 'employee-scheduler-on-demand'); ?></a>
					</div>
				<?php } ?>
				<p><a href="#" class="repeat-availability"><?php _e('Add Another', 'employee-scheduler-on-demand'); ?></a></p>
				<span class="description" style="display:block;clear:both"><?php _e('Enter the days and times you are regularly available.  This will be used to determine your availability for on-demand shifts.', 'employee-scheduler-on-demand' ); ?></span>
			</td>
		</tr>
	<?php }

	/**
	 * Save availability data
	 *
	 * @param $user_id
     */
	public function save_employee_availability_fields( $user_id ) {

		if( isset( $_POST['availability'] ) ) {
			delete_user_meta( $user_id, 'availability' );
			update_user_meta( $user_id, 'availability', $_POST['availability'] );
		}

	}


	/**
	 * Employer Account Shortcode
	 *
	 * @return string
     */
	function employer_account_shortcode() {

		if( is_user_logged_in() && ( $this->check_user_role( 'employer' ) || $this->check_user_role( 'administrator' ) ) ) {
			$user = wp_get_current_user();
			$page_id = get_the_id();

			$shifts = new WP_Query( array(
				'connected_type' => 'shifts_to_employers',
				'connected_items' => $user->ID,
				'nopaging' => true,
				'meta_key' => '_wpaesm_date',
				'orderby' => 'meta_value',
				'order' => 'DESC'
			) );

			if( isset( $_POST['esod_cancelled_shift_id'] ) && '' !== $_POST['esod_cancelled_shift_id'] ) {
				$message = $this->cancel_shift_request();
				if( '' !== $message ) {
					echo $message;
				}
			}

			if ( $shifts->have_posts() ) {

				ob_start();

				include 'partials/shortcode-employer-account.php';

				return ob_get_clean();

				wp_reset_postdata();

			} else {
				_e( 'There are no shifts associated with your account.', 'employee-scheduler-on-demand' );
			}

		} else {
			return $this->require_login( 'employer' );
		}
	}

	/**
	 * Display and process the shift request form
	 *
	 * @param $atts
	 *
	 * @return string|void
	 */
	public function employer_shift_request_form_shortcode( $atts ) {

		$output = '';

		// Attributes
		extract( shortcode_atts(
			array(
					'form' => '',
			), $atts )
		);

		wp_enqueue_style( 'employee-scheduler', plugins_url() . '/employee-scheduler/css/employee-scheduler.css' );
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-scheduler/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-scheduler/js/wpaesmscripts.js', 'jQuery' );

		$user = wp_get_current_user();

		if( isset( $_POST['employer-shift-request'] ) && 'Submit' == $_POST['employer-shift-request'] ) {
			$good = $this->validate_employer_input();

			if( 'good' == $good ) {

				if( isset( $_POST['same-employee'] ) && 'same-employee' == $_POST['same-employee'] ) {
					$possibilities = array();
					foreach( $_POST['when'] as $key => $shift_time ) {
						$possibilities[]  = $this->find_available_employees();
					}
					$result = call_user_func_array( 'array_intersect', $possibilities );
					foreach( $_POST['when'] as $key => $shift_time ) {
						$_POST['when'][$key]['results'] = $result;
					}
				}

				foreach( $_POST['when'] as $key => $shift_time ) {
					$_POST['when'][$key]['results'] = $this->find_available_employees();
				}

				ob_start();
					include 'partials/employer-shift-request-confirmation.php';
				return ob_get_clean();

			} else {
				echo '<p class="esod-error">' . $good . '</p>';
			}
		}

		if( isset( $_POST['esod-shift-request-confirm'] ) ) {

			$good = $this->validate_employer_confirmation();
			if( 'good' == $good ) {
				$created_shifts = $this->create_unassigned_shifts();
				$number_of_shifts = count( $created_shifts );

				$message = '<p>' . sprintf( __( 'Sucess!  %s shift(s) were created, and employees have been notified.  You will receive an email confirmation as soon as someone accepts your shift', 'employee-scheduler-on-demand' ), $number_of_shifts ) . '</p>';

				echo $message;
			} else {
				echo '<p class="esod-error">' . $good . '</p>';
			}

			return;

		}

		if( '' !== $form ) {
			$meta = $this->get_form_meta( $form );
		}

		if( is_user_logged_in() && ( $this->check_user_role( 'employer' ) || $this->check_user_role( 'administrator' ) ) ) {
			ob_start();

			include 'partials/shortcode-employer-shift-request-form.php';

			return ob_get_clean();
		} else {
			return $this->require_login( 'employer' );
		}

	}

	/**
	 * Make sure data we get from the shift request form is usable
	 *
	 * @return string|void
     */
	private function validate_employer_input() {

		// @todo - this would be prettier as an ajax function
		$good = 'good';

		$nonce = $_POST['esod-employer-nonce'];
		if ( ! wp_verify_nonce( $nonce, 'esod-employer-shift-request' ) ) {
			$good = __( 'Permission Error', 'employee-scheduler-on-demand' );

			return $good;
		}

		if ( isset( $this->esod_options['esod_terms'] ) && '' !== $this->esod_options['esod_terms'] ) {
			if ( ! isset( $_POST['terms'] ) || 'agree-to-terms' !== $_POST['terms'] ) {
				$good = __( 'You must accept the terms and conditions to proceed.', 'employee-scheduler-on-demand' );

				return $good;
			}
		}

		$whens = $_POST['when'];
		if( !is_array( $whens ) ) {
			$good = __( 'You must enter dates, start times, and end times for each shift.', 'employee-scheduler-on-demand' );
		}

		foreach( $whens as $when ) {
			// make sure start time is before end time
			if( !isset( $when['start'] ) || !isset( $when['end'] ) ) {
				$good = __( 'You must enter both a start time and an end time.', 'employee-scheduler-on-demand' );
				return $good;
			} elseif( $when['start'] > $when['end'] ) {
				$good = __( 'The start time must be before the end time.', 'employee-scheduler-on-demand');
				return $good;
			}
			// make sure date is in the future
			if( !isset( $when['day'] ) ) {
				$good = __( 'You must enter a date.', 'employee-scheduler-on-demand' );
				return $good;
			} else {
				$shiftday = strtotime( $when['day'] );
				$today = strtotime( date( 'Y-m-d') );
				if( $shiftday < $today ) {
					$good = __( 'You must enter a date in the future.', 'employee-scheduler-on-demand' );
					return $good;
				}
			}
		}

		return $good;
	}

	/**
	 * Validate response from shift request confirmation
	 *
	 * @return string|void
	 */
	private function validate_employer_confirmation() {
		// verify nonce
		$good = 'good';

		$nonce = $_POST['esod-confirmation-nonce'];
		if ( !wp_verify_nonce( $nonce, 'esod-employer-shift-request-confirmation' ) ) {
			$good =  __( 'Permission Error', 'employee-scheduler-on-demand' );
			return $good;
		}
		// check that we have a valid employer id
		$employer = get_user_by( 'id', $_POST['esod-employer'] );
		if( !$employer ) {
			$good = __( 'We could not find your user account.  Please log in and try again.', 'employee-scheduler-on-demand' );
		}

		return $good;
	}

	/**
	 * Search for employees that match requested criteria
	 *
	 * @return array
     */
	private function find_available_employees() {

		$args = array(
			'role' => 'employee',
			'number' => '-1',
		);

		if( isset( $_POST['meta'] ) && is_array( $_POST['meta'] ) ) {
			$args['meta_query'] = array(
					'relation' => 'AND',
			);
			foreach( $_POST['meta'] as $metakey => $metavalue ) {
				if( !is_array( $metavalue ) && '' !== $metavalue ) {
					$args['meta_query'][] = array(
							'key'     => $metakey,
							'value'   => $metavalue,
							'compare' => 'LIKE'
					);
				}
				if( is_array( $metavalue ) && !empty( $metavalue ) ) {
					foreach( $metavalue as $value ) {
						$args['meta_query'][] = array(
								'key'     => $metakey,
								'value'   => $value,
								'compare' => 'LIKE'
						);
					}
				}
			}
		}

		$user_query = new WP_User_Query( $args );

		$result = array();

		if ( !empty( $user_query->results ) ) {

			$i = 0;

			foreach ( $user_query->results as $user ) {

				if( $this->employee_is_available( $user->ID ) ) {
					$i++;
					$result['available_employees'][] = $user->ID;
				}

			}

			if( 0 == $i ) {
				$result['status'] = 'failure';
				$result['msg'] = __( 'We found employees who match your criteria, but none of them are available to work during your desired time(s).  Please change your criteria or schedule and try again.', 'employee-scheduler-on-demand' );
			} else {
				$result['status'] = 'success';
				$result['msg']    = sprintf( __( 'We found %s available employee(s).', 'employee-scheduler-on-demand' ), $i );
			}
		} else {
			$result['status'] = 'failure';
			$result['msg'] = __( 'No employees found.  Please change your criteria and try again.', 'employee-scheduler-on-demand' );
		}

		return $result;
	}

	/**
	 * Check to see if an employee is available during the requested shift time
	 *
	 * @param $user_id
	 *
	 * @return bool
     */
    private function employee_is_available( $user_id ) {

		$availability = get_user_meta( $user_id, 'availability' );

		foreach( $_POST['when'] as $when ) {
			$weekday = strtolower( date( 'l', strtotime( $when['day'] ) ) );

			if( is_array( $availability ) && !empty( $availability ) ) {
				foreach( $availability[0] as $available ) {
					if( $available['day'] == $weekday ) {
						if( ( strtotime( $when['start'] ) >= strtotime( $available['start'] ) ) && ( strtotime( $when['end'] ) <= strtotime( $available['end'] ) ) ) {
							return true;
						}
					}
				}
			}
		}

		return false;

	}

	/**
	 * Create shifts
	 */
	private function create_unassigned_shifts() {

		$details = unserialize( base64_decode( $_POST['esod-request-details'] ) );

		$created_shifts = array();

		if( is_array( $details['when'] ) && !empty( $details['when'] ) ) {

			foreach ( $details['when'] as $shift_time ) {

				$employer = get_user_by( 'id', $details['user_id'] );

				$content = $this->generate_shift_content( $details );

				$shift_args = array(
					'post_type'    => 'shift',
					'post_title'   => 'Shift Requested by ' . $employer->display_name,
					'post_status'  => 'publish',
					'post_content' => $content,
				);
				$shift = wp_insert_post( $shift_args );

				$created_shifts[] = $shift;

				wp_set_object_terms( $shift, 'on-demand', 'shift_type' );
				wp_set_object_terms( $shift, 'unassigned', 'shift_status' );

				// weirdness to make WP Alchemy work
				$fields = array(
					'_wpaesm_employeenote',
					'_wpaesm_date',
					'_wpaesm_starttime',
					'_wpaesm_endtime',
					'_wpaesm_notify',
					'_wpaesm_clockin',
					'_wpaesm_clockout',
					'_wpaesm_location_in',
					'_wpaesm_location_out',
					'_wpaesm_location_in',
					'_wpaesm_location_out'
				);
				$str    = $fields;
				update_post_meta( $shift, 'shift_meta_fields', $str );

				add_post_meta( $shift, '_wpaesm_date', $shift_time['day'] );
				add_post_meta( $shift, '_wpaesm_starttime', $shift_time['start'] );
				add_post_meta( $shift, '_wpaesm_endtime', $shift_time['end'] );

				// connect shift to employer
				p2p_type( 'shifts_to_employers' )->connect( $shift, $employer->ID, array(
					'date' => current_time( 'mysql' )
				) );

				do_action( 'esod-notify-employees-requested-shifts', $shift_time, $shift );

				if ( isset( $this->esod_options['esod_billing_timing'] ) && 'after_request' == $this->esod_options['esod_billing_timing'] ) {
					$admin = new On_Demand_Admin( $this->plugin_name, $this->version );
					$admin->create_order( $shift );
				}

			}

			do_action( 'esod-notify-admins-created-shifts', $created_shifts );

		} else {
			$error = '<p class="esod-error">' . __( 'No shifts were created', 'employee-scheduler-on-demand' ) . '</p>';
			wp_die( $error );
		}

		return $created_shifts;

	}


	/**
	 * Given the shift request form submission, create the shift content
	 *
	 * @return string
	 */
	private function generate_shift_content( $details ) {

		$employer = get_user_by( 'id', $_POST['user_id'] );
		$now = current_time( 'g:i a, M j, Y' );

		$content = '<p>' . sprintf( __( 'Shift requested by %s at %s', 'employee-scheduler-on-demand' ), $employer->display_name, $now ) . '</p>';

		if( isset( $details['location'] ) && '' !== $details['location'] ) {
			$content .= '<p>' . sanitize_text_field( $details['location'] ) . '</p>';
		}

		if( isset( $details['message'] ) && '' !== $details['message'] ) {
			$content .= '<p>' . sanitize_text_field( $details['message'] ) . '</p>';
		}

		if( isset( $details['meta'] ) && is_array( $details['meta'] ) ) {
			$content .= '<p>' . __( 'The Employer requested the following criteria:', 'employee-scheduler-on-demand' ) . '</p><ul>';
			foreach( $details['meta'] as $meta => $value ) {
				$fieldname = end( explode( 'form_', $meta ) );
				$content .= '<li><strong>' . $this->stripped_field_name( $fieldname ) . ':</strong> ' . sanitize_text_field( $value );
			}
			$content .= '</ul>';
		}

		return $content;

	}

	/**
	 * Determine whether or not a shift can be cancelled
	 * Do our options allow for shift cancellation?  Is it in the right timeframe?
	 *
	 * @param $shift
	 *
	 * @return bool
	 */
	private function request_can_be_cancelled( $shift ) {

		if( has_term( 'cancelled', 'shift_status', $shift ) ) {
			return false;
		}

		if( isset( $this->esod_options['esod_allow_cancellation'] ) && 'allow' == $this->esod_options['esod_allow_cancellation'] ) {
			$start_date = get_post_meta( $shift, '_wpaesm_date', true );
			$start_time = get_post_meta( $shift, '_wpaesm_starttime', true );
			$timeframe = 3600 * $this->esod_options['esod_cancellation_deadline'];
			$deadline = strtotime( $start_date . ' ' . $start_time ) - $timeframe;
			$now = time();
			if( $now >= $deadline ) {
				// the deadline has passed, so we can't cancel the request
				return false;
			} else {
				// the deadline hasn't happened yet, so we can cancel the request
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Cancel a shift request: change shift status and remove employee
	 *
	 * @return string
	 */
	private function cancel_shift_request() {

		$result = array();

		$nonce = $_POST['esod-cancel-shift-nonce'];
		if ( !wp_verify_nonce( $nonce, 'esod-cancel-shift-request' ) ) {
			return '<p class="esod-error">' . __( 'Permission Error', 'employee-scheduler-on-demand' ) . '</p>';
		}

		$users = get_users( array(
			'connected_type' => 'shifts_to_employees',
			'connected_items' => $_POST['esod_cancelled_shift_id']
		) );
		if( is_array( $users ) && !empty( $users ) ) {
			do_action( 'esod_notify_employee_cancelled_shift', $users[0]->ID, $_POST['esod_cancelled_shift_id'] );
		}

		// add "cancelled" shift status
		wp_set_object_terms( $_POST['esod_cancelled_shift_id'], 'cancelled', 'shift_status' );
		// remove employee
		p2p_type( 'shifts_to_employees' )->disconnect( $_POST['esod_cancelled_shift_id'], $users[0]->ID );

		$message = '<p class="' . $result['status'] . '">' . $result['message'] . '</p>';
		return $message;

	}

	/**
	 * Notify employee and cc admin when a shift is cancelled
	 *
	 * @param $user_id
	 * @param $shift_id
	 */
	public function notify_employee_cancelled_shift( $user_id, $shift_id ) {

		$from = wpaesm_email_from();

		$user = get_user_by( 'id', $user_id );
		$to = $user->user_email;

		$cc = wpaesm_email_from();

		$subject = __( 'Your Shift Has Been Cancelled', 'employee-scheduler-on-demand' );

		$date_and_time = get_post_meta( $shift_id, '_wpaesm_date', true ) . ', ' . get_post_meta( $shift_id, '_wpaesm_starttime', true ) . ' - ' . get_post_meta( $shift_id, '_wpaesm_endtime', true );
		$message_text = '<p>' . sprintf( __( 'The shift scheduled for %s has been cancelled by the employer.  <a href="%s">View shift details</a>.', 'employee-scheduler-on-demand' ), $date_and_time, get_the_permalink( $shift_id ) ) . '</p>';

		wpaesm_send_email( $from, $to, $cc, $subject, $message_text );

	}

	/**
	 * Filter single shift view to add a link to the invoice
	 *
	 * @param $content
	 * @param $shift
	 * @param $employeeid
	 *
	 * @return string
	 */
	public function esod_single_shift_view( $content, $shift, $employeeid ) {

		if( wpaesm_check_user_role( 'administrator' ) || wpaesm_check_user_role( 'employer' ) ) {

			$invoice = get_post_meta( get_the_id(), '_wpaesm_invoice', true );
			if( isset( $invoice ) && '' !==( $invoice ) ) {
				if( is_numeric( $invoice ) ) {
					$order = new WC_Order( $invoice );
					$invoiceurl = $order->get_checkout_payment_url();
					// @todo - echo currency
					$invoicelink = __( 'Invoice:', 'employee-scheduler-on-demand' ) . ' <a href="' . $invoiceurl . '">' . $order->get_total() . ', ' . $order->get_status() . '</a>';
				} elseif( TRUE === filter_var( $invoice, FILTER_VALIDATE_URL ) ) {
					$invoicelink = '<a href="' . $invoice . '">' . __( 'View Invoice', 'employee-scheduler-on-demand' ) . '</a>';
				} else {
					$invoicelink = __( 'No Invoice Found', 'employee-schedule-manager' );
				}
			}

			$content = $content . $invoicelink;
		}

		return $content;

	}
}
