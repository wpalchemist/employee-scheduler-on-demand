<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://employee-scheduler.co
 * @since      1.0.0
 *
 * @package    On_Demand
 * @subpackage On_Demand/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    On_Demand
 * @subpackage On_Demand/includes
 * @author     Morgan Kay <hello@employee-scheduler.co>
 */
class On_Demand {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      On_Demand_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'employee-scheduler-on-demand';
		$this->version = '1.0.0';
		$this->esod_options = get_option( 'wpaesm_options' );

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - On_Demand_Loader. Orchestrates the hooks of the plugin.
	 * - On_Demand_i18n. Defines internationalization functionality.
	 * - On_Demand_Admin. Defines all hooks for the admin area.
	 * - On_Demand_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-on-demand-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-on-demand-i18n.php';

		/**
		 * The class responsible for the options page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-on-demand-options.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-on-demand-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-on-demand-public.php';

		$this->loader = new On_Demand_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the On_Demand_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new On_Demand_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new On_Demand_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_admin, 'register_cpt_on_demand_form', 0 );
		$this->loader->add_action( 'init', $plugin_admin, 'create_metabox' );
		$this->loader->add_action( 'show_user_profile', $plugin_admin, 'show_criteria_user_meta' );
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'show_criteria_user_meta' );
		$this->loader->add_action( 'p2p_init', $plugin_admin, 'connect_employers_to_shifts' );

		$this->loader->add_action( 'admin_notices', $plugin_admin, 'woocommerce_nag' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_dismiss_woocommerce_nag', $plugin_admin, 'dismiss_woocommerce_nag' );

		$this->loader->add_action( 'esod-notify-employees-requested-shifts', $plugin_admin, 'send_shift_notification_email', 10, 2 );
		$this->loader->add_action( 'wpaesm_extra_shift_fields', $plugin_admin, 'shift_metabox_fields', 10, 1 );
		$this->loader->add_action( 'wpaesp-take-shift-action', $plugin_admin, 'notify_employer_shift_taken', 10, 2 );

		if( isset( $this->esod_options['esod_admin_notification'] ) && 'after_shift' == $this->esod_options['esod_admin_notification'] ) {
			$this->loader->add_action( 'esod-notify-admins-created-shifts', $plugin_admin, 'notify_admins_created_shifts', 10, 1 );
		}

		if( isset( $this->esod_options['esod_send_invoice'] ) && 'automatically' == $this->esod_options['esod_send_invoice'] ) {
			$this->loader->add_action( 'esod-after-create-order', $plugin_admin, 'send_invoice_to_employer', 10, 2 );
		}

		if( isset( $this->esod_options['esod_billing_timing'] ) && 'after_shift' == $this->esod_options['esod_billing_timing'] ) {
			$this->loader->add_action( 'wpaesm_clock_out_action', $plugin_admin, 'create_order', 10, 1 );
		}

		if( ( isset( $this->esod_options['esod_billing_timing'] ) && 'notify' == $this->esod_options['esod_billing_timing'] ) ||
			!isset( $this->esod_options['esod_billing_timing'] ) ) {
			// default is to create invoice when employee accepts shift
			$this->loader->add_action( 'wpaesp-take-shift-action', 10, 2 );
		}

		$options = new On_Demand_Options();
		$this->loader->add_filter( 'wpaesm_validation_options_filter', $options, 'sanitize_options' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new On_Demand_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wpaesm_additional_user_profile_fields', $plugin_public, 'employee_availability_fields', 10, 2 );
		$this->loader->add_action( 'wpaesm_save_additional_user_profile_fields', $plugin_public, 'save_employee_availability_fields', 10, 2 );
		$this->loader->add_filter( 'wpaesm_filter_single_shift_view', $plugin_public, 'esod_single_shift_view', 10, 4 );
		$this->loader->add_action( 'esod_notify_employee_cancelled_shift', $plugin_public, 'notify_employee_cancelled_shift', 10, 2 );

		add_shortcode( 'employee_work_criteria_form', array( $plugin_public, 'employee_work_criteria_form_shortcode' ) );
		add_shortcode( 'employer_account', array( $plugin_public, 'employer_account_shortcode' ) );
		add_shortcode( 'employer_shift_request_form', array( $plugin_public, 'employer_shift_request_form_shortcode' ) );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    On_Demand_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}



}
