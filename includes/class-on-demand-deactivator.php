<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wpalchemists.com
 * @since      1.0.0
 *
 * @package    On_Demand
 * @subpackage On_Demand/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    On_Demand
 * @subpackage On_Demand/includes
 * @author     Morgan Kay <hello@employee-scheduler.co>
 */
class On_Demand_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
