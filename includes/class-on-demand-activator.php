<?php

/**
 * Fired during plugin activation
 *
 * @link       https://employee-scheduler.co
 * @since      1.0.0
 *
 * @package    On_Demand
 * @subpackage On_Demand/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    On_Demand
 * @subpackage On_Demand/includes
 * @author     Morgan Kay <hello@employee-scheduler.co>
 */
class On_Demand_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		add_role( 'employer', 'Employer', array( 'read' => true, 'edit_posts' => false, 'publish_posts' => false ) );

	}

}
